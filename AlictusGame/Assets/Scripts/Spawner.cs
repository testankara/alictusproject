using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Spawn Object Prefab")]
    public GameObject spawnPrefab;

    [Header("Insert position")]
    public Vector3 center;

    [Header("Insert size")]
    public Vector3 size;

    [Header("Number of spawn")]
    public int SpawnNum;

    private OliveCounter _oliveCounter;

    private Vector3 temporaryPos;

    [Header("With Level")]
    public int level;

    private void Start()
    {
        _oliveCounter = FindObjectOfType<OliveCounter>();
        Spawn();
    }

    private void Spawn()
    {
        for(int i=0;i<SpawnNum;i++)
        {
            
            Vector3 _pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2));
            if(level == 2)
            {
                float distance = Vector3.Distance(_pos, temporaryPos);
                if (distance < 1f)
                {
                    i++;
                    continue;
                }
                temporaryPos = _pos;
            }
            Quaternion _rot = Quaternion.Euler(0, 0, Random.Range(-180, 180));
            Instantiate(spawnPrefab, _pos, _rot);
            if(level == 1)
            {
                _oliveCounter.AddOlive();
            }
            print(i);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 1, 0, 1);
        Gizmos.DrawCube(center, size);
    }
}
