﻿using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{

    [Header("Position Settings")]
    [SerializeField] bool enableTranslation = true;
    [SerializeField] Vector3 movementVector;
    [SerializeField] float positionPeriod = 3f;
    [SerializeField] bool moveOnce;

    [Header("Rotation Settings")]
    [SerializeField] bool enableRotation = false;
    [SerializeField] Vector3 rotationVector;
    [SerializeField] float rotaionSpeed = 3f;

    float movementFactor;

    Vector3 startingPos;

    void Start()
    {
        startingPos = transform.position;
    }

    void Update()
    {
        if (enableTranslation) Translate();
        if (enableRotation) Rotate();
    }

    private void Translate()
    {
        if (positionPeriod <= Mathf.Epsilon) return;

        float cycles = Time.time / positionPeriod;

        const float tau = Mathf.PI * 2;
        float rawSinWave = Mathf.Sin(cycles * tau);

        movementFactor = rawSinWave / 2f + 0.5f;
        Vector3 offset = movementFactor * movementVector;
        transform.position = startingPos + offset;

        if (transform.position == startingPos + movementVector && moveOnce)
        {
            enabled = false;
        }
    }

    private void Rotate()
    {
        transform.Rotate(rotationVector * rotaionSpeed * Time.deltaTime);
    }
}
