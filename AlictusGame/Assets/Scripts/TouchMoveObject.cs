﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMoveObject : MonoBehaviour
{
    private bool isMove = false;

    private RaycastHit _hit;

    [Header("Limit move in scene")]
    public float[] xLimit;
    public float[] yLimit;

    private void Update()
    {
        if(Input.GetMouseButton(0))
        {
            ObjectContactControl();
        }
        else if(Input.GetMouseButtonUp(0))
        {
            isMove = false;
        }
        if (isMove)
        {
            MoveObject();
        }
        ControlLimit();
    }

    private void ControlLimit()
    {
        if(transform.position.x < xLimit[0])
        {
            FreezeInBorder(xLimit[0], "x");
        }

        if (transform.position.x > xLimit[1])
        {
            FreezeInBorder(xLimit[1], "x");
        }

        if (transform.position.y < yLimit[0])
        {
            FreezeInBorder(yLimit[0], "y");
        }

        if (transform.position.y > yLimit[1])
        {
            FreezeInBorder(yLimit[1], "y");
        }
    }

    private void FreezeInBorder(float limit , string xy)
    {
        if(xy == "x")
        {
            transform.position = new Vector3(limit, transform.position.y, transform.position.z);
        }

        if(xy == "y")
        {
            transform.position = new Vector3(transform.position.x, limit, transform.position.z);
        }
    }

    private void MoveObject()
    {
        Plane objPlane = new Plane(Camera.main.transform.forward, transform.position);

        Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        float rayDistance;

        if (objPlane.Raycast(mRay, out rayDistance))
        {
            transform.position = new Vector3(mRay.GetPoint(rayDistance).x, mRay.GetPoint(rayDistance).y, transform.position.z);
        }
    }

    private void ObjectContactControl()
    {
        Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(_ray, out _hit))
        {
            if (_hit.collider.gameObject == gameObject)
            {
                isMove = true;
            }
        }
    }
}
