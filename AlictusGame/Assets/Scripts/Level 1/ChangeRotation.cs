using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRotation : MonoBehaviour
{

    private Touch touch;
    
    private Quaternion down;
    private Quaternion up;

    private void Start()
    {
        down = Quaternion.Euler(transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z);
        up = Quaternion.Euler(transform.localEulerAngles.x + 180, transform.localEulerAngles.y, transform.localEulerAngles.z);
    }

    private void Update()
    {
        RotateFunc();
    }

    private void RotateFunc()
    {
        if(Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Moved)
            {
                if (touch.deltaPosition.y < 0.5f)
                {
                    transform.localRotation = Quaternion.Lerp(transform.localRotation, down, 0.25f);
                }
                else if (touch.deltaPosition.y > 0.5f && touch.deltaPosition.x > 0.5f)
                {
                    transform.localRotation = Quaternion.Lerp(transform.localRotation,up,0.25f);
                }
            }
        }
    }
}
