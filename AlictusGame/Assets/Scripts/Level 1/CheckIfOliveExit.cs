using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckIfOliveExit : MonoBehaviour
{

    private OliveCounter _oliveCounter;

    private void Start()
    {
        _oliveCounter = FindObjectOfType<OliveCounter>();
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Olive")
        {
            print("Exit");
            _oliveCounter
                .RemoveOlive();
        }
    }
}
