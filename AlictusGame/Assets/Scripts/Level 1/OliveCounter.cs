using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OliveCounter : MonoBehaviour
{
    private int oliveCount = 0;
    private int startCount = 0;

    [Header("Counter Bar")]
    public Image bar;

    [Header("Quantity to be ignored")]
    public int ignoreQuantity;

    private FinalManager finalManager;

    private void Start()
    {
        finalManager = GetComponent<FinalManager>();
    }

    public void AddOlive()
    {
        oliveCount += 1;
        startCount += 1;
    }

    public void RemoveOlive()
    {
        oliveCount -= 1;
        bar.fillAmount += 1.0f / (startCount - ignoreQuantity);
        if((oliveCount - ignoreQuantity) < 1)
        {
            finalManager.ChangeScene();
        }
    }
}
