using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveLevelOneContactControl : MonoBehaviour
{
    private Vector3 stopPos;
    private bool isRemove = false;

    private OliveCounter _oliveCounter;

    private void Start()
    {
        _oliveCounter = FindObjectOfType<OliveCounter>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Remover" && !isRemove)
        {
            if (_oliveCounter == null) return;
            
            _oliveCounter.RemoveOlive();

            isRemove = true;
            stopPos = transform.position;
        }
    }

    private void Update()
    {
        if(isRemove)
        {
            transform.position = stopPos;
        }
    }
}
