using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{

    [Header("Force power")]
    public float force;

    private Rigidbody rgd;

    private bool isStart = false;

    private void Start()
    {
        rgd = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if(!isStart)
        {
            if(Input.GetMouseButtonDown(0))
            {
                isStart = true;
                rgd.AddForce(new Vector3(1f, 0.5f, 0) * force);
            }
        }
    }
}
