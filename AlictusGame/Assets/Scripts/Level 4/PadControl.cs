using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadControl : MonoBehaviour
{
    private bool isMove = false;

    private RaycastHit _hit;

    [Header("Limit move in scene")]
    public float[] xLimit;

    public bool isReverse = false;

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            ObjectContactControl();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isMove = false;
        }
        if (isMove)
        {
            MoveObject();
        }
        ControlLimit();
    }

    private void ControlLimit()
    {
        if (transform.position.x < xLimit[0])
        {
            FreezeInBorder(xLimit[0], "x");
        }

        if (transform.position.x > xLimit[1])
        {
            FreezeInBorder(xLimit[1], "x");
        }
    }

    private void FreezeInBorder(float limit, string xy)
    {
        if (xy == "x")
        {
            transform.position = new Vector3(limit, transform.position.y, transform.position.z);
        }
    }

    private void MoveObject()
    {
        Plane objPlane = new Plane(Camera.main.transform.forward, transform.position);

        Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        float rayDistance;

        if (objPlane.Raycast(mRay, out rayDistance))
        {
            if(!isReverse)
            {
                transform.position = new Vector3(mRay.GetPoint(rayDistance).x, transform.position.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(-mRay.GetPoint(rayDistance).x, transform.position.y, transform.position.z);
            }
        }
    }

    private void ObjectContactControl()
    {
        Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(_ray, out _hit))
        {
            if (_hit.collider.gameObject == gameObject)
            {
                isMove = true;
            }
        }
    }
}
