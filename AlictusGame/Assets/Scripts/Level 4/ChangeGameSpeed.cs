using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeGameSpeed : MonoBehaviour
{
    void Start()
    {
        Time.timeScale = 0.7f;       
    }
    

    public void AddSpeed()
    {
        Time.timeScale = 1f;
    }
}
