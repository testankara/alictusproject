using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallContact : MonoBehaviour
{
    private GameObject pad;

    [Header("Brick Count")]
    public int bricks;

    private FinalManager final;

    private void Start()
    {
        final = FindObjectOfType<FinalManager>();
        pad = FindObjectOfType<PadControl>().gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Cherry")
        {
            FindObjectOfType<ChangeGameSpeed>().AddSpeed();
            Destroy(other.gameObject);
            CountBricks();
        }

        if (other.tag == "Banana")
        {
            pad.transform.localScale += new Vector3(0.2f, 0, 0);
            Destroy(other.gameObject);
            CountBricks();
        }

        if (other.tag == "Hamburger")
        {
            pad.GetComponent<PadControl>().isReverse = true;
            Destroy(other.gameObject);
            CountBricks();
        }

        if (other.tag == "WaterMelon")
        {
            transform.localScale += new Vector3(0.2f, 0, 0);
            Destroy(other.gameObject);
            CountBricks();
        }

        if (other.tag == "Cheese")
        {
            Destroy(other.gameObject);
            CountBricks();
        }

        if (other.tag == "Fail")
        {
            final.RestartScene();
            Destroy(gameObject);
        }
    }

    private void CountBricks()
    {
        bricks -= 1;
        if(bricks <= 0)
        {
            final.ChangeScene();
            Destroy(gameObject);
        }
    }
}
