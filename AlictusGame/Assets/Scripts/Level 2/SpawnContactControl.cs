using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnContactControl : MonoBehaviour
{
    [Header("Spawn object")]
    public GameObject spawnObject;

    public bool enable = true;

    [Header("The Couple string")]
    public string tag;

    private Quaternion rot;

    private bool isContact = false;

    private FinalManager finalManager;

    private void Start()
    {
        finalManager = FindObjectOfType<FinalManager>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Cheese" && this.gameObject.tag == "Olive")
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
        if (collision.collider.tag == tag && !isContact)
        {

            isContact = true;

            if (tag == "Cheese")
            {
                finalManager.ChangeScene();
                return;
            }

            collision.collider.gameObject.GetComponent<SpawnContactControl>().enable = false;

            if (enable)
            {
                if (tag == "olive")
                {
                    rot = Quaternion.Euler(0, 0, Random.Range(-180, 180));
                }
                if (tag == "Cherry")
                {
                    rot = Quaternion.Euler(Random.Range(-180, 180), -90, 180);
                }
                if (tag == "Banana")
                {
                    rot = Quaternion.Euler(45, 90, -90);
                }
                if (tag == "HotDog")
                {
                    rot = Quaternion.Euler(90, 0, 180);
                }
                if (tag == "Hamburger")
                {
                    rot = Quaternion.Euler(0, -90, 180);
                }

                Instantiate(spawnObject, transform.position, rot);
                Destroy(collision.collider.gameObject);
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == tag)
        {
            enable = true;
        }
    }
}
