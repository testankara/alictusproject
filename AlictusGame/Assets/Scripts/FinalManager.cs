using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FinalManager : MonoBehaviour
{

    [Header("Level Completed Text")]
    public Text levelCompleted;

    [Header("Gift Show")]
    public GameObject gift;

    private ParticleSystem confetti;

    private void Start()
    {
        PlayerPrefs.SetInt("Level", SceneManager.GetActiveScene().buildIndex);
        confetti = GameObject.FindWithTag("Confetti").GetComponent<ParticleSystem>();
        levelCompleted.text = "Level " + (SceneManager.GetActiveScene().buildIndex) + "\nCompleted";
    }

    public void ChangeScene()
    {
        levelCompleted.gameObject.SetActive(true);
        confetti.Play();
        gift.SetActive(true);
        StartCoroutine(ChangeSceneAfterTime());
    }

    public void RestartScene()
    {
        levelCompleted.text = "Try Again";
        levelCompleted.gameObject.SetActive(true);
        StartCoroutine(RestartThisLevel());
    }

    IEnumerator RestartThisLevel()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator ChangeSceneAfterTime()
    {
        yield return new WaitForSeconds(7.5f);
        if (SceneManager.GetActiveScene().buildIndex == (SceneManager.sceneCountInBuildSettings - 1))
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
