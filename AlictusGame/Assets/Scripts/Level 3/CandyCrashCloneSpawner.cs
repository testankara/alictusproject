using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyCrashCloneSpawner : MonoBehaviour
{
    private int height = 6;
    private int width = 5;

    [Header("Foods Prefabs")]
    public List<GameObject> foods = new List<GameObject>();

    [HideInInspector] public GameObject click1, click2;

    private GameObject incomeObject;

    private FinalManager final;

    private void Start()
    {
        final = FindObjectOfType<FinalManager>();
        CreateFoods();
    }

    private void CreateFoods()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if(y == 0)
                {
                    if(x == 0)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 1)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 2)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 3)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 4)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                }
                if (y == 1)
                {
                    if (x == 0)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 1)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 2)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 3)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 4)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                }
                if (y == 2)
                {
                    if (x == 0)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 1)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 2)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 3)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 4)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                }
                if (y == 3)
                {
                    if (x == 0)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 1)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 2)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 3)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 4)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                }
                if (y == 4)
                {
                    if (x == 0)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 1)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 2)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 3)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 4)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                }
                if (y == 5)
                {
                    if (x == 0)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 1)
                    {
                         incomeObject = Instantiate(foods[0], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 2)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 3)
                    {
                         incomeObject = Instantiate(foods[2], new Vector3(x, y, 2), Quaternion.identity);
                    }
                    if (x == 4)
                    {
                         incomeObject = Instantiate(foods[1], new Vector3(x, y, 2), Quaternion.identity);
                    }
                }
            }
        }
    }

    public void Calling()
    {
        float xDistance = Mathf.Abs(click1.transform.position.x - click2.transform.position.x);
        float yDistance = Mathf.Abs(click1.transform.position.y - click2.transform.position.y);

        if(xDistance <= 1 && yDistance <= 1)
        {
            Vector3 clicked1 = click1.transform.position;
            Vector3 clicked2 = click2.transform.position;

            click1.transform.position = clicked2;
            click2.transform.position = clicked1;
        }

        foreach (var allObj in FindObjectsOfType<CandyClick>())
        {
            allObj.Control();
        }

        click1 = null;
        click2 = null;
    }

    public void SearchFoods()
    {
        StartCoroutine(CheckIfFoodsFinish());
    }

    IEnumerator CheckIfFoodsFinish()
    {
        yield return new WaitForSeconds(1.5f);
        List<GameObject> foodsInScene = new List<GameObject>();

        foreach (CandyClick item in FindObjectsOfType<CandyClick>())
        {
            foodsInScene.Add(item.gameObject);
        }

        if (foodsInScene.Count <= 1)
        {
            final.ChangeScene();
        }
    }

}
