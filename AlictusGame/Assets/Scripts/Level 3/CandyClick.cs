using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyClick : MonoBehaviour
{
    [HideInInspector]public CandyCrashCloneSpawner candySpawner;

    [Header("Object ID")]
    public int id;

    public List<GameObject> allObject = new List<GameObject>();
    public List<GameObject> nearObjectsX = new List<GameObject>();
    public List<GameObject> nearObjectsY = new List<GameObject>();

    private void Awake()
    {
        candySpawner = GameObject.FindWithTag("CandySpawner").GetComponent<CandyCrashCloneSpawner>();
    }

    public void Control()
    {
        foreach (var allObj in FindObjectsOfType<CandyClick>())
        {
            if (allObj.gameObject != gameObject)
            {
                allObject.Add(allObj.gameObject);
            }

            float x = Mathf.Abs(transform.position.x - allObj.gameObject.transform.position.x);
            float y = Mathf.Abs(transform.position.y - allObj.gameObject.transform.position.y);

            if ((x == 1 && y == 0))
            {
                if (allObj.GetComponent<CandyClick>().id == id)
                {
                    nearObjectsX.Add(allObj.gameObject);
                }
            }

            if ((x == 0 && y == 1))
            {
                if (allObj.GetComponent<CandyClick>().id == id)
                {
                    nearObjectsY.Add(allObj.gameObject);
                }
            }
        }

        bool isDestroyed = false;

        if(nearObjectsX.Count >= 2)
        {
            foreach (GameObject item in nearObjectsX)
            {
                Destroy(item);
            }
            isDestroyed = true;
        }

        if (nearObjectsY.Count >= 2)
        {
            foreach (GameObject item in nearObjectsY)
            {
                Destroy(item);
            }
            isDestroyed = true;
        }

        if (isDestroyed)
        {
            candySpawner.SearchFoods();
            Destroy(gameObject);
        }

        allObject.Clear();
        nearObjectsX.Clear();
        nearObjectsY.Clear();
    }

    private void OnMouseDown()
    {
        if(candySpawner.click1 == null)
        {
            candySpawner.click1 = gameObject;
        }
        else
        {
            candySpawner.click2 = gameObject;
            candySpawner.Calling();
        }
    }
}
